using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelChanger : MonoBehaviour
{
    public Material material;
    public List<Mesh> meshes= new List<Mesh>();
    int index;
    void Start()
    {
        material = GetComponent<MeshRenderer>().material;
    }
[ContextMenu("ChangeColor")]
    public void changeMat()
    {
        material.SetColor("_BaseColor", Random.ColorHSV());
    }
    [ContextMenu("ChangeMesh")]
    public void ChangeMesh()
    {
        if (meshes.Count == 0)
            return;
        GetComponent<MeshFilter>().mesh = meshes[index];
        index++;
        if (meshes.Count <= index)
            index = 0;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
