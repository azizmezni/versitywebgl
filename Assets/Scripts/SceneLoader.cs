

using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
   
    public bool ChangeNow;
    public string sceneName;
    int currentScene;
    public float ChangeTime=0;
    public void Change()
    {
        ChangeNow = true;


    }
    private void Start()
    {
      
    }
    public void LoadSceneInBackground()
    {
       
        if(!string.IsNullOrEmpty(sceneName))
        StartCoroutine(LoadScene());
    }

    IEnumerator LoadScene()
    {
       
        yield return null;

        //Begin to load the Scene you specify

        currentScene= SceneManager.GetActiveScene().buildIndex;
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneName);
        //Don't let the Scene activate until you allow it to
        asyncOperation.allowSceneActivation = false;
      
        //When the load is still in progress, output the Text and progress bar
        while (!asyncOperation.isDone)
        {
            //Output the current progress
            //m_Text.text = "Loading progress: " + (asyncOperation.progress * 100) + "%";

            // Check if the load has finished
            if (asyncOperation.progress >= 0.9f)
            {
                //Change the Text to show the Scene is ready
                //m_Text.text = "Press the space bar to continue";
                //Wait to you press the space key to activate the Scene
                if (ChangeNow)
                {  //Activate the Scene
                    yield return new WaitForSeconds(1f);
                    asyncOperation.allowSceneActivation = true;
                }
            }
            SceneManager.UnloadSceneAsync(currentScene);
            Resources.UnloadUnusedAssets();
            yield return null;
        }
    }
}