using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class FPSCalculator : MonoBehaviour
{
    public TextMeshProUGUI textMesh;
    // Start is called before the first frame update
    void Start()
    {
        if (textMesh == null)
            textMesh = GetComponent<TextMeshProUGUI>();
        
    }

    // Update is called once per frame
    void Update()
    {
        textMesh.text=( (int)(1f / Time.unscaledDeltaTime)).ToString()+" FPS";
    }
}
