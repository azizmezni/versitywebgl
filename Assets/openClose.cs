using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class openClose : MonoBehaviour
{
    public GameObject GameObjectToOpenClose;
    public bool defaultValue;
    void Start()
    {
        if (GameObjectToOpenClose == null)
        {
            Debug.LogError("Forget serialise Field on " + gameObject.name);
            return;

        }
        GameObjectToOpenClose.SetActive(defaultValue);
    }
    public void Click()
    {
        if (GameObjectToOpenClose == null)
            Debug.LogError("Forget serialise Field on " + gameObject.name);
        GameObjectToOpenClose.SetActive(!GameObjectToOpenClose.activeSelf);


    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
