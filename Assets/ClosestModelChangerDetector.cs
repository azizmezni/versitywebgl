using UnityEngine;

public class ClosestModelChangerDetector : MonoBehaviour
{
    public static ClosestModelChangerDetector instance;
    public ModelChanger modelChanger;
    private Camera cam;
    public float fieldOfView = 60f;
    public bool run;
    private void Awake()
    {
        instance = this;
        cam = Camera.main;
    }
   
    
    private void Update()
    {if (!run)
            return;
        Collider[] colliders = Physics.OverlapSphere(transform.position, 10f);
        float closestDistance = Mathf.Infinity;
        GameObject closestObject = null;

        foreach (Collider collider in colliders)
        {
            if (collider.GetComponent<ModelChanger>() != null)
            {
                Vector3 direction = collider.transform.position - cam.transform.position;
                if (Vector3.Angle(direction, cam.transform.forward) < fieldOfView * 0.5f)
                {
                    float distance = Vector3.Distance(transform.position, collider.transform.position);
                    if (distance < closestDistance)
                    {
                        closestDistance = distance;
                        closestObject = collider.gameObject;
                    }
                }
            }
        }

        if (closestObject != null)
        {
            ModelChanger closestModelChanger = closestObject.GetComponent<ModelChanger>();
            if (closestModelChanger != null)
            {
                if (!closestModelChanger.Equals(modelChanger))
                {
                    DeselectModel();
                    modelChanger = closestModelChanger;
                }
                if (modelChanger.GetComponent<Outline>())
                {

                }
                else
                {
                var outline=    modelChanger.gameObject.AddComponent<Outline>();
                    outline.OutlineMode = Outline.Mode.OutlineVisible;
                }
            }
        }
        else
        {
            DeselectModel();
        }
    }

    public void DeselectModel()
    {
        if (modelChanger != null)
            if (modelChanger.TryGetComponent(out Outline m_outline))
            {
                modelChanger = null;
                Destroy(m_outline);
            }
    }
}