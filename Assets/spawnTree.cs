using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnTree : MonoBehaviour
{
    public List<Three> threes = new List<Three>();
    public List<GameObject> parrents = new List<GameObject>();
    public List<GameObject> spawnedTree = new List<GameObject>();

    [ContextMenu("Spawn")]
    public void spawn()
    {
      
           
            for (int i = spawnedTree.Count-1; i >0 ; i--)
            {
                DestroyImmediate(spawnedTree[i].gameObject);
            }

        spawnedTree.Clear();
        foreach (var item in parrents)
        {
            var refence = threes[Random.Range(0, threes.Count)];
         var obj=   Instantiate(refence.prefabs, item.transform);
            spawnedTree.Add(obj);
            obj.transform.localPosition = refence.localposition;
            obj.transform.localEulerAngles = refence.LocalRotation;


        }
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
[System.Serializable]
public class Three
{
    public GameObject prefabs;
    public Vector3 localposition;
    public Vector3 LocalRotation;

}