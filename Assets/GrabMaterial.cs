using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabMaterial : MonoBehaviour
{
    public List<Material> materials = new List<Material>();
    public List<string> ShaderName = new List<string>();
    public List<Renderer> renderers = new List<Renderer>();
    [ContextMenu("Grab")]
   public void grab()
    {
        materials.Clear();
        ShaderName.Clear();
           var rend = FindObjectsOfType<Renderer>(true);

        foreach (var item in rend)
        {
            foreach (var mat in item.materials)
            {
                if (!mat.shader.name.Contains("Lit")) {
                   
                materials.Add(mat);
                ShaderName.Add(mat.shader.name);
                }
            }
        }
        materials.Sort();

    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
