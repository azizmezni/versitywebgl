using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditHelper : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }
    private void OnEnable()
    {
        ClosestModelChangerDetector.instance.run = true;
    }
    private void OnDisable()
    {
        
        ClosestModelChangerDetector.instance.run = false;
        ClosestModelChangerDetector.instance.DeselectModel();
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
